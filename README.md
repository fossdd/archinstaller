## archinstaller

This is a basic kept arch linux installer.

It only downloads and installs required packages without bloat like editors or so.

This script won't receive more "features". Only bug fixes, that it could receive updates.

If you want to use a installer that has more features then look to [`codeberg.org/fossdd/archscript`](https://codeberg.org/fossdd/archscript).

### Using

Go in your `archiso` live environment and type:

```bash
curl https://gitlab.com/fossdd/archinstaller/-/raw/dev/install.sh > install.sh
chmod +x install.sh
./install.sh
```