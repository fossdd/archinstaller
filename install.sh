#!/usr/bin/env bash

RED="\e[1;31m"
GREEN="\e[1;32m"
YELLOW="\e[1;33m"
BLUE="\e[1;34m"
CYAN="\e[1;36m"
BOLD="\e[1;37m"
NC="\e[0m"

set -e

print_fatal() {
	echo -e "\n${RED}\n==> ${CYAN}$@${NC}"
	exit 1
}

print_prompt() {
	echo -ne "\n${YELLOW}\n==> ${CYAN}$@${NC}"
}

print_step() {
    clear
    echo -ne "${BOLD}==> ${CYAN}$@${NC}\n"
}

print_sucess() {
    echo -ne "\n${GREEN}\n==> ${CYAN}$@${NC}"
}

# Config: Boot
lsblk
DEFAULT="/dev/sda"
print_prompt "Disk [${DEFAULT}]: "
read DISKPATH
DISKPATH=${DISKPATH:-${DEFAULT}}
[[ ! -b "$DISKPATH" ]] && print_fatal "Device does not exist. Exiting."

# Check if this is an EFI system and plan accordingly
BOOTLOADER="bios"
[[ -d "/sys/firmware/efi" ]] && BOOTLOADER="efi"

# Config: Filesystem
DEFAULT="ext4"
print_prompt "Filesystem [${DEFAULT}]: "
read FILESYSTEM
FILESYSTEM=${FILESYSTEM:-${DEFAULT}}
! command -v mkfs.$FILESYSTEM &> /dev/null && print_fatal "Filesystem type does not exist. Exiting."

# Config: Timezone
DEFAULT=UTC
print_prompt "Timezone [${DEFAULT}]: "
read TIMEZONE
TIMEZONE=${TIMEZONE:-${DEFAULT}}
[[ ! -f "/usr/share/zoneinfo/$TIMEZONE" ]] && print_fatal "/usr/share/zoneinfo/$TIMEZONE does not exist. Exiting."

# Config: Hostname
DEFAULT=localhost
print_prompt "Hostname [${DEFAULT}]: "
read HOSTNAME
HOSTNAME=${HOSTNAME:-${DEFAULT}}

# Config: Root Password
print_prompt "Root Password: "
read -s PASSWORD
[[ ! ${PASSWORD} ]] && print_fatal "Password is empty. Exiting."

# Config: Boot & Root
BOOT="${DISKPATH}1"
ROOT="${DISKPATH}2"

# Verify
echo
echo
printf "%-16s\t%-16s\n" "CONFIGURATION" "VALUE"
printf "%-16s\t%-16s\n" "Bootloader:" "$BOOTLOADER"
printf "%-16s\t%-16s\n" "Device:" "$DISKPATH"
printf "%-16s\t%-16s\n" "Boot Partition:" "$BOOT"
printf "%-16s\t%-16s\n" "Root Filesystem:" "$FILESYSTEM"
printf "%-16s\t%-16s\n" "Root Partition:" "$ROOT"
printf "%-16s\t%-16s\n" "Timezone:" "$TIMEZONE"
printf "%-16s\t%-16s\n" "Hostname:" "$HOSTNAME"
printf "%-16s\t%-16s\n" "Password:" "`echo \"$PASSWORD\" | sed 's/./*/g'`"
echo ""
print_prompt "Proceed? [y/n]: "
read PROCEED
[[ "$PROCEED" != "y" ]] && print_fatal "User chose not to proceed. Exiting."
print_sucess "You've done all configuration setups. Now just wait until the installation is finished"

# Unmount for safety
print_step Unmount partitions
[[ "$BOOTLOADER" == "efi" ]] && umount "$BOOT" 2> /dev/null || true
umount "$ROOT" 2> /dev/null || true

# Timezone
timedatectl set-ntp true

# Partitioning
print_step Automatic partitioning
(
	echo g		# Erase as GPT

	# EFI or BIOS partitions
	if [[ "$BOOTLOADER" == "efi" ]]
	then
		echo n
		echo
		echo
		echo +512M
		echo t
		echo 1
	else
		echo n
		echo
		echo
		echo +1M
		echo t
		echo 4
	fi

	echo n		# Linux root
	echo
	echo
	echo
	sleep 3		# Delay to avoid race condition
	echo w		# Write
) | fdisk -w always -W always "$DISKPATH"

# Formatting partitions
print_step Create Filesystems
[[ "$BOOTLOADER" == "efi" ]] && mkfs.fat -F 32 "$BOOT"
yes | mkfs.$FILESYSTEM "$ROOT"

# Mount our new partition
print_step Mount Root Filesystem
mount "$ROOT" /mnt

# Initialize base system, kernel, and firmware
print_step Install Packages to Root Filesystem
pacstrap /mnt base linux linux-firmware

# Setup fstab
print_step Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Chroot commands


# Time and date configuration
print_step "Setup time and date"
(
	echo "ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime"
	echo "hwclock --systohc"
) | arch-chroot /mnt


# Setup locales
print_step "Setup locales"
(
	echo "sed -i \"s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/\" /etc/locale.gen"
	echo "locale-gen"
	echo "echo \"LANG=en_US.UTF-8\" > /etc/locale.conf"
) | arch-chroot /mnt

# Setup hostname and hosts file
print_step "Setup hostname"
(
	echo "echo \"$HOSTNAME\" > /etc/hostname"
	echo "echo -e \"127.0.0.1\tlocalhost\" >> /etc/hosts"
	echo "echo -e \"::1\t\tlocalhost\" >> /etc/hosts"
	echo "echo -e \"127.0.1.1\t$HOSTNAME\" >> /etc/hosts"
	echo "echo -e \"$PASSWORD\n$PASSWORD\" | passwd"
) | arch-chroot /mnt

# Install microcode
print_step "Install microcode"
(
	echo "pacman -Sy --noconfirm amd-ucode intel-ucode"
) | arch-chroot /mnt

# Install GRUBv2 as a removable drive (universal across hw)
if [[ "$BOOTLOADER" == "efi" ]]
then
	print_step "Install UEFI Grub"
	(
		echo "pacman -Sy --noconfirm grub efibootmgr"
		echo "mkdir /boot/efi"
		echo "mount \"$BOOT\" /boot/efi"
		echo "grub-install --efi-directory=/boot/efi --bootloader-id=GRUB --removable"
		echo "grub-mkconfig -o /boot/grub/grub.cfg"
	) | arch-chroot /mnt
else
	print_step "Install BIOS Grub"
	(
		echo "pacman -Sy --noconfirm grub"
		echo "grub-install --removable \"$DISKPATH\""
		echo "grub-mkconfig -o /boot/grub/grub.cfg"
	) | arch-chroot /mnt
fi

# Install and enable NetworkManager on boot
print_step "Install NetworkManager"
(
	echo "pacman -Sy --noconfirm networkmanager iwd"
	echo "systemctl enable NetworkManager"
) | arch-chroot /mnt

print_sucess "Install completed on $DISKPATH." 
print_sucess "You are now ready to restart your new system!"